const express = require('express');

const Category = require("../models/Category");

const router = express.Router();

const createRouter = () => {
  router.get('/', async (req, res) => {
    try {
      const postData = await Category.find();
      return res.send(postData);
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.post('/', async (req, res) => {
    try {
      const postData = new Category(req.body);
      if (!postData) return res.status(400).send({ message: 'Bad Request' });

      postData.save().then(
        response => { res.send(postData) },
        error => { res.status(500).send({ error, message: 'Internal Server Error' }) }
      );
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.put('/:id', async (req, res) => {
    try {
      let postData = await Category.findById(req.params.id);
      if (!postData) return res.status(404).send({ message: 'Not Found' });

      postData = Object.assign(postData, req.body);
      postData.save().then(
        response => { res.send(postData) },
        error => { res.status(500).send({ error, message: 'Internal Server Error' }) }
      )
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.delete('/:id', async (req, res) => {
    try {
      const postData = await Category.findById(req.params.id);
      if (!postData) return res.status(404).send({ message: 'Not Found' });

      postData.remove().then(
        response => { res.status(200).send({ message: 'Success' }) },
        error => { res.status(500).send({ error, message: 'Internal Server Error' }) }
      );
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  return router;
}

module.exports = createRouter;
