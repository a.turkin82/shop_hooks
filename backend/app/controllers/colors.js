const express = require('express');

const Color = require('../models/Color');

const router = express.Router();

const createRouter = () => {
  router.get('/', async (req, res) => {
    try {
      const colors = await Color.find();
      return res.send(colors);
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  return router;
}

module.exports = createRouter;
