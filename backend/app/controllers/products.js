const express = require('express');
const config = require('../../config');

const upload = require('../middlewares/upload');

const Product = require("../models/Product");

const router = express.Router();

const createRouter = () => {
  router.get('/', async (req, res) => {
    try {
      const postData = await Product.find();
      return res.send(postData);
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.post('/', upload.single('image'), async (req, res) => {
    try {
      const postData = new Product(req.body);
      if (!postData) return res.status(400).send({ message: 'Bad Request' });

      if (req.file) {
        postData.image = config.uploadUrl + req.file.filename;
      }

      postData.save().then(
        response => { res.send(postData) },
        error => { res.status(500).send({ error, message: 'Internal Server Error' }) }
      );
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.put('/:id', upload.single('image'), async (req, res) => {
    try {
      let postData = await Product.findById(req.params.id);
      if (!postData) return res.status(404).send({ message: 'Not Found' });

      postData = Object.assign(postData, req.body);
      if (req.file) {
        postData.image = config.uploadUrl + req.file.filename;
      }

      postData.save().then(
        response => { res.send(postData) },
        error => { res.status(500).send({ error, message: 'Internal Server Error' }) }
      )
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.delete('/:id', async (req, res) => {
    try {
      const postData = await Product.findById(req.params.id);
      if (!postData) return res.status(404).send({ message: 'Not Found' });

      postData.remove().then(
        response => { res.status(200).send({ message: 'Success' }) },
        error => { res.status(500).send({ error, message: 'Internal Server Error' }) }
      );
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  return router;
}

module.exports = createRouter;
