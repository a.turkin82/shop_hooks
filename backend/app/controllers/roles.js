const express = require('express');

const auth = require("../middlewares/auth");
const permit = require("../middlewares/permit");

const Role = require('../models/Role');

const router = express.Router();

const createRouter = () => {
  router.get('/', auth, permit('admin'), async (req, res) => {
    try {
      const roles = await Role.find();
      return res.send(roles);
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  return router;
}

module.exports = createRouter;
