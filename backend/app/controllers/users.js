const express = require('express');

const auth = require("../middlewares/auth");
const permit = require("../middlewares/permit");
const token = require("../middlewares/token");

const Role = require('../models/Role');
const User = require('../models/User');

const router = express.Router();

const createRouter = () => {
  router.post('/auth', async (req, res) => {
    if (!req.body.email) {
      return res.status(400).send({ message: 'Bad Request' });
    }

    try {
      let user = await User.findOne({email: req.body.email});

      if (!user) {
        user = new User(req.body);
      } else {
        const isMatch = await user.checkPassword(req.body.password);
        if (!isMatch) {
          return res.status(401).send({error: 'Unauthenticated'});
        }
      }

      user.save().then(
        response => {res.send({user, token: token(user)})},
        error => {res.status(500).send({ error, message: 'Internal Server Error' })}
      );
    }
    catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.get('/auth-as-guest', async (req, res) => {
    try {
      const role = await Role.findOne({ name: 'guest' });
      const user = new User({email: 'Anonymous', password: 'Anonymous', role: role._id});
      user.save().then(
        response => {res.send({user, token: token(user)})},
        error => {res.status(500).send({ error, message: 'Internal Server Error' })}
      );
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.get('/logout', auth, async (req, res) => {
    try {
      const user = await User.findById(req.userId);
      if (user) {
        token(user);
        return res.send({ message: 'Success' });
      } else {
        return res.status(401).send({error: 'Unauthenticated'});
      }
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  router.get('/auth', auth, async (req, res) => {
    try {
      const user = await User.findById(req.userId).populate('role');
      if (user) {
        return res.send(user);
      } else {
        return res.status(401).send({error: 'Unauthenticated'});
      }
    } catch (error) {
      return res.status(500).send({ error, message: 'Internal Server Error' });
    }
  });

  return router;
}

module.exports = createRouter;
