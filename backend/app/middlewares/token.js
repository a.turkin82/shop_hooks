const jwt = require('jsonwebtoken');
const config = require('../../config');

function token(user) {
  return token = jwt.sign({id: user._id}, config.jwt.secret, {
    expiresIn: config.jwt.lifetime
  });
}

module.exports = token;
