const multer = require('multer');
const path = require('path');
const uuid = require("uuid");

const config = require('../../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    const hash = uuid.v4().replace(/-/g, '');
    cb(null, hash + path.extname(file.originalname));
  }
});

module.exports = multer({storage});
