const User = require('../models/User');

const permit = (...perms) => {
  return async (req, res, next) => {
    if (!req.userId) return res.status(401).send({ message: 'Unauthenticated' });

    const user = await User.findById(req.userId).populate('role');

    if (!user) return res.status(401).send({ message: 'Unauthenticated' });

    if (!perms.includes(user.role?.name)) return res.status(403).send({ message: 'Forbidden' });

    next();
  }
};

module.exports = permit;
