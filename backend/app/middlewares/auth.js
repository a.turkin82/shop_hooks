const jwt = require('jsonwebtoken');
const config = require('../../config');

async function auth(req, res, next) {
  const token = req.headers['x-access-token'];
  if (!token) return res.status(401).send({ message: 'Unauthorized' });

  jwt.verify(token, config.jwt.secret, function(error, decoded) {
    if (error)
      return res.status(401).send({error: 'Unauthenticated'});

    req.userId = decoded.id;
    next();
  });
}

module.exports = auth;
