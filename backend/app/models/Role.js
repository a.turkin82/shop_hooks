const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoleSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  created_at: {
    type: String,
    default: new Date().toISOString(),
    required: true
  }
});

module.exports = mongoose.model('Role', RoleSchema);
