const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ColorSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  created_at: {
    type: String,
    default: new Date().toISOString(),
    required: true
  }
});

module.exports = mongoose.model('Color', ColorSchema);
