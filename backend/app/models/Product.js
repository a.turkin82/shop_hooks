const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  article: {
    type: String,
    required: true
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  color: {
    type: Schema.Types.ObjectId,
    ref: 'Color'
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  price: {
    type: String || Number,
    required: true
  },
  delivery: {
    type: Boolean,
    default: false,
    required: true
  },
  image: {
    type: String
  },
  created_at: {
    type: String,
    default: new Date().toISOString(),
    required: true
  }
});

ProductSchema.pre('save', async function (next) {
  if (!this.isModified('price')) return next();

  const price = Number(this.price);
  if (!isNaN(price)) this.price = price;
  else throw new Error('Bad Request');

  next();
});

module.exports = mongoose.model('Product', ProductSchema);
