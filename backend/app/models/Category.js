const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  created_at: {
    type: String,
    default: new Date().toISOString(),
    required: true
  }
});

module.exports = mongoose.model('Category', CategorySchema);
