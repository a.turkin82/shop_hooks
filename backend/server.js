const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');

const categories = require('./app/controllers/categories');
const products = require('./app/controllers/products');
const roles = require('./app/controllers/roles');
const settings = require('./app/controllers/settings');
const users = require('./app/controllers/users');

const app = express();

const port = process.env.PORT || 8000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/categories', categories());
  app.use('/products', products());
  app.use('/roles', roles());
  app.use('/settings', settings());
  app.use('/users', users());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
