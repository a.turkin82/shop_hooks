const mongoose = require("mongoose");
const config = require("./config");

const Category = require('./app/models/Category');
const Color = require('./app/models/Color');
const Product = require('./app/models/Product');
const Role = require('./app/models/Role');
const User = require('./app/models/User');

mongoose.connect(config.db.url + '/' + config.db.name, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

console.log('##### start');

db.once("open", async () => {
  try {
    await db.dropCollection("categories");
    console.log("Collection Categories was dropped !");
  }
  catch (e) {
    console.log("Collection Categories not present, skipping drop...");
  }
  try {
    await db.dropCollection("colors");
    console.log("Collection Colors was dropped !");
  }
  catch (e) {
    console.log("Collection Colors not present, skipping drop...");
  }
  try {
    await db.dropCollection("products");
    console.log("Collection Products was dropped !");
  }
  catch (e) {
    console.log("Collection Products not present, skipping drop...");
  }
  try {
    await db.dropCollection("roles");
    console.log("Collection Roles was dropped !");
  }
  catch (e) {
    console.log("Collection Roles not present, skipping drop...");
  }
  try {
    await db.dropCollection("users");
    console.log("Collection Users was dropped !");
  }
  catch (e) {
    console.log("Collection Users not present, skipping drop...");
  }

  const [cat] = await Category.create([
    {
      name: 'Компьютеры и оргтехника',
      description: 'Компьютеры и их комплектующие, офисная техника'
    }
  ]);

  const [c1, c2, c3, c4, c5, c6, c7, c8] = await Color.create([
    { name: 'Черный' },
    { name: 'Белый' },
    { name: 'Красный' },
    { name: 'Зеленый' },
    { name: 'Синий' },
    { name: 'Желтый' },
    { name: 'Серый' },
    { name: 'Малиновый' }
  ]);

  await Product.create([
    {
      article: 12345,
      category: cat._id,
      color: c7._id,
      name: 'Процессор Core i3',
      description: 'CPU Intel Core i3-10105, LGA1200, 3.7-4.4Hz, 6MB Cache L3, UHD 630, EMT64,4 Cores + 8 Threads,Tray,Comet Lake',
      price: 9535,
      image: 'Intel-Core-i3-10105.jpg'
    }
  ]);

  const [r1, r2, r3, r4] = await Role.create([
    { name: 'admin' },
    { name: 'manager' },
    { name: 'user' },
    { name: 'guest' }
  ]);

  await User.create([
    {
      email: 'admin@admin.com',
      password: 'admin',
      role: r1._id,
      first_name: 'Админ',
      last_name: 'Админыч'
    },
    {
      email: 'manager@manager.com',
      password: 'manager',
      role: r2._id,
      first_name: 'Менеджер',
      last_name: 'Манагерович'
    }
  ]);

  db.close();

  console.log('##### finish');
});
