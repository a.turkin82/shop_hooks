const path = require('path');
const rootPath = __dirname;

module.exports = {
  db: {
    url: 'mongodb://' + (process.env.MONGO_HOST || 'localhost') + ':27017',
    name: 'shop_kb'
  },
  jwt: {
    lifetime: 86400000,
    secret: 'thisjwtisvery[jhjibq'
  },
  rootPath,
  uploadUrl: '/uploads/',
  uploadPath: path.join(rootPath, '/public/uploads')
};
