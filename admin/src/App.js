import React from 'react';
import {Route, Routes} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import Login from "./pages/Login";

const App = () => {
  return (
    <Routes>
      <Route path='/login' exact name="Login" element={<Login />} />
      <Route path='*' element={<Layout />} />
    </Routes>
  );
}

export default App;
