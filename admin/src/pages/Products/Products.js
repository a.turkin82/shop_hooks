import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Container, Row, Table} from "react-bootstrap";

import loader from '../../assets/images/loading-light.gif';
import EditProductModal from "../../components/Modals/ProductsModals/EditProductModal";

import {createProduct, fetchProductsList} from "../../store/actions/products";
import {fetchCategoriesList} from "../../store/actions/categories";
import {fetchColorsList} from "../../store/actions/colors";

const Products = () => {
  const dispatch = useDispatch();

  const categoriesList = useSelector(state => state.categories.categoriesList);
  const colors = useSelector(state => state.colors.colorsList);
  const productsList = useSelector(state => state.products.productsList);

  const loadingCategories = useSelector(state => state.categories.loadingList);
  const loadingColors = useSelector(state => state.colors.loadingList);
  const loadingProducts = useSelector(state => state.products.loadingList);

  const [loading, setLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (!(categoriesList && categoriesList.length)) dispatch(fetchCategoriesList());
    if (!(colors && colors.length)) dispatch(fetchColorsList());
    if (!(productsList && productsList.length)) dispatch(fetchProductsList());
  }, []);

  useEffect(() => {
    if ((loadingCategories || loadingColors || loadingProducts) && !loading) setLoading(true);
    if (!loadingCategories && !loadingColors && !loadingProducts && loading) setLoading(false);
    // eslint-disable-next-line
  }, [loadingCategories, loadingColors, loadingProducts]);

  const onToggleModal = action => {
    setShowModal(!!action);
  }

  const formSubmit = (event, data) => {
    event.preventDefault();

    const formData = new FormData();
    for (let key in data) formData.append(key, data[key]);

    dispatch(createProduct(formData, () => {
      onToggleModal(false);
      dispatch(fetchProductsList());
    }));
  };

  const createProductRow = (el) => {
    const category = categoriesList.find(e => e.id === el.category);
    const color = colors.find(e => e.id === el.color);

    let delivery = 'Нет';
    if (String(el.delivery) === 'true' || String(el.delivery) === '1') delivery = 'Да';

    return (
      <tr key={el.id}>
        <td>{el.article}</td>
        <td>{category ? category.title : 'N/A'}</td>
        <td>{el.title}</td>
        <td>{el.price}</td>
        <td>{color ? color.title : 'N/A'}</td>
        <td>{delivery}</td>
      </tr>
    )
  };

  return (
    <Container>
      <Row>
        <Col xs={12} className="mb-4">
          <Button variant="success" onClick={() => onToggleModal(true)}>
            Создать товар
          </Button>
        </Col>
      </Row>

      <Row>
        <Col xs={12}>
          <Table bordered striped>
            <thead>
            <tr>
              <th>Артикул</th>
              <th>Категория</th>
              <th>Название</th>
              <th>Цена</th>
              <th>Цвет</th>
              <th>Доставка</th>
            </tr>
            </thead>
            <tbody>
            {productsList.map(el => createProductRow(el))}
            </tbody>
          </Table>

          {loading ?
            <div className="loader-wrapper">
              <img src={loader} alt=""/>
            </div>
            : null}
        </Col>
      </Row>

      <EditProductModal show={showModal}
                        toggle={onToggleModal}
                        onOk={formSubmit}
                        size="lg"
                        categoriesList={categoriesList}
                        colors={colors}
      />

    </Container>
  );
};

export default Products;
