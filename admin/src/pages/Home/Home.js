import React from 'react';
import {Container} from "react-bootstrap";

import catImg1 from '../../assets/images/categories/dry-mixes.jpg';
import catImg2 from '../../assets/images/categories/power-tools.png';
import catImg3 from '../../assets/images/categories/plumbing.png';
import catImg4 from '../../assets/images/categories/roof.jpg';
import catImg5 from '../../assets/images/categories/various.jpg';

const categoriesPromo = [
  { title: 'Сухие смеси', image: catImg1 },
  { title: 'Электроинструмент', image: catImg2 },
  { title: 'Сантехника', image: catImg3 },
  { title: 'Кровельные материалы', image: catImg4 },
  { title: 'Разное', image: catImg5 },
];

const Home = () => {
  const createCategoryPromoItem = (data, key) => {
    return (
      <div key={key} className="categories-promo__item">
        <div className="categories-promo__image">
          <img src={data.image} alt={data.title}/>
        </div>
        <div className="categories-promo__content">
          <div className="categories-promo__title">
            {data.title}
          </div>
          <div className="categories-promo__description">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam animi debitis deleniti deserunt dicta dolorum, eveniet fuga id impedit magni nam, non, officiis omnis porro quae quas quisquam recusandae rerum veritatis voluptatibus. Dignissimos dolore ducimus iure labore, possimus quisquam velit?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam animi debitis deleniti deserunt dicta dolorum, eveniet fuga id impedit magni nam, non, officiis omnis porro quae quas quisquam recusandae rerum veritatis voluptatibus. Dignissimos dolore ducimus iure labore, possimus quisquam velit?
          </div>
        </div>
      </div>
    )
  };

  return (
    <div className="main-page">
      <div className="categories-promo__wrapper">
        {categoriesPromo.map((el, idx) => createCategoryPromoItem(el, 'catItem' + idx))}
      </div>
    </div>
  );
};

export default Home;
