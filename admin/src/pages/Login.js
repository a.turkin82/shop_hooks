import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
import {Button, Form, InputGroup} from "react-bootstrap";

import logo from '../assets/images/logo.png';

import {login, loginAsGuest} from "../store/actions/auth";

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [form, setForm] = useState({
    email: '',
    password: ''
  });

  const inputChangeHandler = event => {
    const state = {...form};
    state[event.target.name] = event.target.value;
    setForm(state);
  };

  const formSubmitHandler = event => {
    event.preventDefault();
    dispatch(login(form, () => {
      navigate('/');
    }));
  };

  const signAsGuestHandler = event => {
    event.preventDefault();
    dispatch(loginAsGuest(() => {
      navigate('/');
    }));
  };

  return (
    <div className="login-page">
      <div className="login-page__logo-wrapper" style={{backgroundImage: `url(${logo})`}}/>
      <div className="login-form__wrapper">
        <h3 className="login-form__title">
          Авторизация
        </h3>
        <h6 className="login-form__subtitle">
          Чтобы продолжить необходимо авторизоваться
        </h6>

        <Form onSubmit={formSubmitHandler}>
          <InputGroup className="mt-3 mb-3">
            <InputGroup.Text id="email">
              <i className="fa fa-envelope-o" aria-hidden="true"/>
            </InputGroup.Text>
            <Form.Control type="email"
                          name="email"
                          placeholder="E-mail"
                          value={form.email}
                          onChange={inputChangeHandler}
                          required
            />
          </InputGroup>

          <InputGroup className="mt-3 mb-3">
            <InputGroup.Text id="password">
              <i className="fa fa-key" aria-hidden="true"/>
            </InputGroup.Text>
            <Form.Control type="password"
                          name="password"
                          placeholder="Пароль"
                          value={form.password}
                          onChange={inputChangeHandler}
                          required
            />
          </InputGroup>

          <Form.Group className="mt-3 d-flex justify-content-between">
            <Button className="btn btn-gold" type="submit">Авторизация</Button>
            <Button className="btn btn-gold" onClick={signAsGuestHandler}>Войти как гость</Button>
          </Form.Group>
        </Form>
      </div>
    </div>
  );
};

export default Login;
