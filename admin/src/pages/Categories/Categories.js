import React, {useEffect, useState} from 'react';
import {Button, Col, Container, Row, Table} from "react-bootstrap";

import loader from '../../assets/images/loading-light.gif';
import EditCategoryModal from "../../components/Modals/CategoriesModals/EditCategoryModal";

import {useDispatch, useSelector} from "react-redux";

import {createCategory, deleteCategory, fetchCategoriesList, updateCategory} from "../../store/actions/categories";

const Categories = () => {
  const dispatch = useDispatch();

  const [showModal, setShowModal] = useState(false);
  const [editingItem, setEditingItem] = useState({});

  const categoriesList = useSelector(state => state.categories.categoriesList);
  const loading = useSelector(state => state.categories.loadingList);

  useEffect(() => {
    // if (!(categoriesList && categoriesList.length))
      dispatch(fetchCategoriesList());
  }, []);

  const onToggleModal = action => {
    setShowModal(!!action);
  }

  const formSubmit = (event, data) => {
    event.preventDefault();

    if (data.id) dispatch(updateCategory(data.id, data, () => dispatch(fetchCategoriesList())));
    else dispatch(createCategory(data, () => dispatch(fetchCategoriesList())));

    setEditingItem({});
    onToggleModal(false);
  }

  const onEditHandler = (el) => {
    setEditingItem(el);
    onToggleModal(true);
  }

  const onDeleteHandler = async (id) => {
    dispatch(deleteCategory(id, () => dispatch(fetchCategoriesList())));
  }

  const createCategoryRow = (el) => {
    return (
      <tr key={el.id}>
        <td>{el.title}</td>
        <td>{el.description}</td>
        <td>
          <span onClick={() => onEditHandler(el)}>Редактировать</span>
          <br/>
          <span onClick={() => onDeleteHandler(el.id)}>Удалить</span>
        </td>
      </tr>
    );
  };

  return (
    <Container>
      <Row>
        <Col xs={12} className="mb-4">
          <Button variant="success" onClick={() => onEditHandler({})}>
            Создать категорию
          </Button>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <Table bordered striped>
            <thead>
            <tr>
              <th>Название</th>
              <th>Описание</th>
              <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            {categoriesList.map(el => createCategoryRow(el))}
            </tbody>
          </Table>

          {loading ?
            <div className="loader-wrapper">
              <img src={loader} alt=""/>
            </div>
            : null}
        </Col>
      </Row>

      <EditCategoryModal show={showModal}
                         toggle={onToggleModal}
                         size="lg"
                         onOk={formSubmit}
                         defaultData={editingItem}
      />

    </Container>
  );
}

export default Categories;
