import {getPathByName} from "./_routes";

const mainNav = [
  {
    name: 'Products',
    title: 'Главная',
    path: getPathByName('Products')
  },
  {
    name: 'Categories',
    title: 'О нас',
    path: getPathByName('Categories')
  },
  {
    name: 'Products',
    title: 'Товары',
    path: getPathByName('Products')
  },
  {
    name: 'Categories',
    title: 'Категории',
    path: getPathByName('Categories')
  },
  {
    name: 'Categories',
    title: 'Контакты',
    path: getPathByName('Categories')
  }
];

export default mainNav;
