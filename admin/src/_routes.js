import Home from "./pages/Home/Home";
import Products from "./pages/Products/Products";
import Categories from "./pages/Categories/Categories";
import Page404 from "./pages/Page404/Page404";

const routes = [
  { path: '/', exact: true, name: 'Home', element: Home },
  { path: '/products', exact: true, name: 'Products', element: Products },
  { path: '/categories', exact: true, name: 'Categories', element: Categories },
  { path: '*', name: 'Page404', element: Page404 }
];

export function getPathByName (name) {
  return routes.find(el => el.name === name)?.path;
}

export default routes;
