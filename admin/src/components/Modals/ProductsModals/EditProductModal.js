import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";

const EditProductModal = ({ show, toggle, onOk, size, categoriesList, colors }) => {
  const [form, setForm] = useState({
    article: '',
    category: '',
    title: '',
    price: 0,
    amount: 0,
    color: '1',
    delivery: true,
    image: ''
  });

  const inputChangeHandler = event => {
    const state = {...form};
    state[event.target.name] = event.target.value;
    setForm(state);
  }

  const clearForm = () => {
    setForm({
      article: '',
      category: '',
      title: '',
      price: 0,
      amount: 0,
      color: '1',
      delivery: true,
      image: ''
    });
    toggle(false);
  }

  const checkboxChangeHandler = event => {
    const state = {...form};
    state[event.target.name] = event.target.checked;
    setForm(state);
  }

  const selectFileHandler = event => {
    const state = {...form};
    state[event.target.name] = event.target.files[0];
    setForm(state);
  }

  return (
    <Modal show={show} onHide={() => toggle(false)} size={size || 'md'}>
      <Form onSubmit={(e) => onOk(e, form)}>
        <Modal.Header closeButton>
          <Modal.Title>Редактирование товара</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form.Group controlId="article" className="form-group">
            <Form.Label>Артикул</Form.Label>
            <Form.Control type="text"
                          name="article"
                          placeholder="Артикул"
                          value={form.article}
                          onChange={inputChangeHandler}
              // required
            />
          </Form.Group>

          <Form.Label htmlFor="category">Категория товара</Form.Label>
          <Form.Select id="category"
                       name="category"
                       className="form-group"
                       aria-label="Категория товара"
                       value={form.category}
                       onChange={inputChangeHandler}
            // required
          >
            <option value=''>--- Выберите категорию ---</option>
            {categoriesList.map(el => (
              <option key={'cat' + el.id} value={el.id}>{el.title}</option>
            ))}
          </Form.Select>

          <Form.Group controlId="title" className="form-group">
            <Form.Label>Название товара</Form.Label>
            <Form.Control type="text"
                          name="title"
                          placeholder="Наименование"
                          value={form.title}
                          onChange={inputChangeHandler}
              // required
            />
          </Form.Group>

          <Form.Group controlId="price" className="form-group">
            <Form.Label>Розничная цена</Form.Label>
            <Form.Control type="number"
                          name="price"
                          min={0}
                          value={form.price}
                          onChange={inputChangeHandler}
            />
          </Form.Group>

          <Form.Group controlId="amount" className="form-group">
            <Form.Label>Количество</Form.Label>
            <Form.Control type="number"
                          name="amount"
                          min={0}
                          value={form.amount}
                          onChange={inputChangeHandler}
            />
          </Form.Group>

          <Form.Group className="form-group">
            <Form.Label className="d-block">Выберите цвет</Form.Label>
            {colors.map(el => (
              <Form.Check key={'color' + el.id}
                          inline
                          label={el.name}
                          name="color"
                          type="radio"
                          id={'color' + el.id}
                          checked={form.color === el.id}
                          value={el.id}
                          onChange={inputChangeHandler}
              />
            ))}
          </Form.Group>

          <Form.Group className="form-group">
            <Form.Check type="switch"
                        id="delivery"
                        label="Доставка предусмотрена"
                        name="delivery"
                        checked={form.delivery}
                        onChange={checkboxChangeHandler}
            />
          </Form.Group>

          <Form.Group className="form-group">
            <input type="file"
                   name="image"
                   onChange={selectFileHandler}
            />
          </Form.Group>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={clearForm}>
            Отмена
          </Button>
          <Button type="submit" variant="primary">
            Сохранить
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default EditProductModal;
