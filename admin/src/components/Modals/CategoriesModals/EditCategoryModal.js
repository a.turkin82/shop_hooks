import React, {useEffect, useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";

const EditCategoryModal = ({ show, toggle, onOk, size, defaultData }) => {
  const [form, setForm] = useState({
    title: '',
    description: ''
  });

  useEffect(() => {
    if (defaultData.id) {
      setForm(defaultData);
    }
  }, [defaultData]);

  const inputChangeHandler = event => {
    const state = {...form};
    state[event.target.name] = event.target.value;
    setForm(state);
  }

  const clearForm = () => {
    setForm({
      title: '',
      description: ''
    });
    toggle(false);
  }

  return (
    <Modal show={show} onHide={() => toggle(false)} size={size || 'md'}>
      <Form onSubmit={(e) => onOk(e, form)}>
        <Modal.Header closeButton>
          <Modal.Title>
            {form.id ? 'Редактировать' : 'Создать'} категорию товаров
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form.Group controlId="title" className="form-group">
            <Form.Label>Название</Form.Label>
            <Form.Control type="text"
                          name="title"
                          placeholder="Название"
                          value={form.title}
                          onChange={inputChangeHandler}
                          required
            />
          </Form.Group>

          <Form.Group controlId="description" className="form-group">
            <Form.Label>Описание</Form.Label>
            <Form.Control type="text"
                          name="description"
                          placeholder="Описание"
                          value={form.description}
                          onChange={inputChangeHandler}
                          required
            />
          </Form.Group>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={clearForm}>
            Отмена
          </Button>
          <Button type="submit" variant="primary">
            Сохранить
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default EditCategoryModal;
