import React from 'react';
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {Container} from "react-bootstrap";

import mainNav from "../../_nav";

import {logout} from "../../store/actions/auth";

const MainNav = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onRedirect = (e, path) => {
    e.preventDefault();
    navigate(path);
  };

  const onLogoutHandler = async () => {
    dispatch(logout());
  };

  return (
    <nav className="nav">
      <Container>
        <div className="nav-inner-wrapper">
          <div className="main-nav__aside"/>
          <div className="main-nav">
            {mainNav.map((el, idx) => (
              <a key={"navItem" + el.name + idx}
                 href={el.path}
                 onClick={e => onRedirect(e, el.path)}
              >
                {el.title}
              </a>
            ))}
          </div>
          <div className="main-nav__user">
            <i className="fa fa-sign-out" aria-hidden="true" onClick={onLogoutHandler}/>
          </div>
        </div>
      </Container>
    </nav>
  );
};

export default MainNav;
