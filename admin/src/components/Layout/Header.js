import React from 'react';
import {useNavigate} from "react-router-dom";
import {Container} from "react-bootstrap";

import bgImg from '../../assets/images/header-image.png';
import logo from '../../assets/images/logo.png';
import logoName from '../../assets/images/logo-name.png';
import logoPhone from '../../assets/images/logo-phone.png';

const Header = () => {
  const navigate = useNavigate();

  return (
    <header className="header" style={{backgroundImage: `url(${bgImg})`}}>
      <Container>
        <div className="main-logo"
            onClick={() => {navigate('/')}}
        >
          <div className="main-logo__image-wrapper" style={{backgroundImage: `url(${logo})`}}/>
          <div className="main-logo__name-image-wrapper" style={{backgroundImage: `url(${logoName})`}}/>
          <a href="tel: +996313123456" className="main-logo__phone-image-wrapper"
             style={{backgroundImage: `url(${logoPhone})`}}
             onClick={e => e.stopPropagation()}
          />
        </div>
      </Container>
    </header>
  );
};

export default Header;
