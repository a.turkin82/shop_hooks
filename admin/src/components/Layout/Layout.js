import React from 'react';

import Header from "./Header";
import AppContent from "./AppContent";
import Footer from "./Footer";
import MainNav from "./MainNav";

const Layout = () => {
  return (
    <div className="main-layout">
      <Header/>
      <MainNav/>
      <AppContent/>
      <Footer/>
    </div>
  );
};

export default Layout;
