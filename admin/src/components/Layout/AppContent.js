import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Route, Routes, useNavigate} from "react-router-dom";
import {Container} from "react-bootstrap";

import routes from '../../_routes';
import {auth} from "../../store/actions/auth";

const AppContent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const token = useSelector(state => state.auth.token);

  const authLoading = useSelector(state => state.auth.loading);

  useEffect(() => {
    if (token) {
      dispatch(auth(null, () => {
        navigate('/login');
      }));
    } else {
      navigate('/login');
    }
  }, [token]);

  return (
    <Container className="main-content">
      <aside className="aside">

      </aside>
      <main className="main">
        <Routes>
          {routes.map((el, idx) => (
            <Route key={'route' + idx}
                   path={el.path}
                   exact={!!el.exact}
                   name={el.name}
                   element={<el.element/>}
            />
          ))}
        </Routes>
      </main>
    </Container>
  );
};

export default AppContent;
