import React from 'react';
import {Container} from "react-bootstrap";

import bgImg from '../../assets/images/footer-image.png';

const Footer = () => {
  return (
    <footer className="footer" style={{backgroundImage: `url(${bgImg})`}}>
      <Container className="footer__inner-wrapper">
        FOOTER
      </Container>
    </footer>
  );
};

export default Footer;
