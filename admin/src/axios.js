import axios from 'axios';
import { baseUrl } from "./config";
import store from "./store/configureStore";

const instance = axios.create({
  baseURL: baseUrl,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json'
  }
});

instance.interceptors.request.use(function (config) {
  config.headers['x-access-token'] = store.getState().auth.token;
  return config;
}, function (error) {
  return Promise.reject(error);
});

instance.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  return Promise.reject(error);
});

export default instance;
