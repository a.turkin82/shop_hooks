export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';

export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAILURE = 'AUTH_FAILURE';

export const FETCH_CATEGORIES_LIST_REQUEST = 'FETCH_CATEGORIES_LIST_REQUEST';
export const FETCH_CATEGORIES_LIST_SUCCESS = 'FETCH_CATEGORIES_LIST_SUCCESS';
export const FETCH_CATEGORIES_LIST_FAILURE = 'FETCH_CATEGORIES_LIST_FAILURE';

export const CREATE_CATEGORY_REQUEST = 'CREATE_CATEGORY_REQUEST';
export const CREATE_CATEGORY_SUCCESS = 'CREATE_CATEGORY_SUCCESS';
export const CREATE_CATEGORY_FAILURE = 'CREATE_CATEGORY_FAILURE';

export const UPDATE_CATEGORY_REQUEST = 'UPDATE_CATEGORY_REQUEST';
export const UPDATE_CATEGORY_SUCCESS = 'UPDATE_CATEGORY_SUCCESS';
export const UPDATE_CATEGORY_FAILURE = 'UPDATE_CATEGORY_FAILURE';

export const DELETE_CATEGORY_REQUEST = 'DELETE_CATEGORY_REQUEST';
export const DELETE_CATEGORY_SUCCESS = 'DELETE_CATEGORY_SUCCESS';
export const DELETE_CATEGORY_FAILURE = 'DELETE_CATEGORY_FAILURE';

export const FETCH_PRODUCTS_LIST_REQUEST = 'FETCH_PRODUCTS_LIST_REQUEST';
export const FETCH_PRODUCTS_LIST_SUCCESS = 'FETCH_PRODUCTS_LIST_SUCCESS';
export const FETCH_PRODUCTS_LIST_FAILURE = 'FETCH_PRODUCTS_LIST_FAILURE';

export const CREATE_PRODUCT_REQUEST = 'CREATE_PRODUCT_REQUEST';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';

export const UPDATE_PRODUCT_REQUEST = 'UPDATE_PRODUCT_REQUEST';
export const UPDATE_PRODUCT_SUCCESS = 'UPDATE_PRODUCT_SUCCESS';
export const UPDATE_PRODUCT_FAILURE = 'UPDATE_PRODUCT_FAILURE';

export const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';

export const FETCH_COLORS_LIST_REQUEST = 'FETCH_COLORS_LIST_REQUEST';
export const FETCH_COLORS_LIST_SUCCESS = 'FETCH_COLORS_LIST_SUCCESS';
export const FETCH_COLORS_LIST_FAILURE = 'FETCH_COLORS_LIST_FAILURE';

export const CREATE_COLOR_REQUEST = 'CREATE_COLOR_REQUEST';
export const CREATE_COLOR_SUCCESS = 'CREATE_COLOR_SUCCESS';
export const CREATE_COLOR_FAILURE = 'CREATE_COLOR_FAILURE';

export const UPDATE_COLOR_REQUEST = 'UPDATE_COLOR_REQUEST';
export const UPDATE_COLOR_SUCCESS = 'UPDATE_COLOR_SUCCESS';
export const UPDATE_COLOR_FAILURE = 'UPDATE_COLOR_FAILURE';

export const DELETE_COLOR_REQUEST = 'DELETE_COLOR_REQUEST';
export const DELETE_COLOR_SUCCESS = 'DELETE_COLOR_SUCCESS';
export const DELETE_COLOR_FAILURE = 'DELETE_COLOR_FAILURE';
