import axios from "../../axios";
import * as types from '../actionTypes';

const fetchProductsListRequest = () => ({ type: types.FETCH_PRODUCTS_LIST_REQUEST });
const fetchProductsListSuccess = payload => ({ type: types.FETCH_PRODUCTS_LIST_SUCCESS, payload });
const fetchProductsListFailure = error => ({ type: types.FETCH_PRODUCTS_LIST_FAILURE, error });

export const fetchProductsList = () => {
  return dispatch => {
    dispatch(fetchProductsListRequest());
    return axios.get('/products').then(
      response => {
        dispatch(fetchProductsListSuccess(response.data));
      },
      error => {
        dispatch(fetchProductsListFailure(error))
      }
    )
  }
}

const createProductRequest = () => ({ type: types.CREATE_PRODUCT_REQUEST });
const createProductSuccess = () => ({ type: types.CREATE_PRODUCT_SUCCESS });
const createProductFailure = error => ({ type: types.CREATE_PRODUCT_FAILURE, error });

export const createProduct = (data, onSuccess) => {
  return dispatch => {
    dispatch(createProductRequest());
    return axios.post('/products', data).then(
      response => {
        dispatch(createProductSuccess());
        onSuccess();
      },
      error => {
        dispatch(createProductFailure(error));
      }
    )
  }
}

const updateProductRequest = () => ({ type: types.UPDATE_PRODUCT_REQUEST });
const updateProductSuccess = () => ({ type: types.UPDATE_PRODUCT_SUCCESS });
const updateProductFailure = error => ({ type: types.UPDATE_PRODUCT_FAILURE, error });

export const updateProduct = (id, data, onSuccess) => {
  return dispatch => {
    dispatch(updateProductRequest());
    return axios.put(`/products/${id}`, data).then(
      response => {
        dispatch(updateProductSuccess());
        onSuccess();
      },
      error => {
        dispatch(updateProductFailure(error))
      }
    )
  }
}

const deleteProductRequest = () => ({ type: types.DELETE_PRODUCT_REQUEST });
const deleteProductSuccess = () => ({ type: types.DELETE_PRODUCT_SUCCESS });
const deleteProductFailure = error => ({ type: types.DELETE_PRODUCT_FAILURE, error });

export const deleteProduct = (id, onSuccess) => {
  return dispatch => {
    dispatch(deleteProductRequest());
    return axios.delete(`/products/${id}`).then(
      response => {
        dispatch(deleteProductSuccess());
        onSuccess();
      },
      error => {
        dispatch(deleteProductFailure(error))
      }
    )
  }
}
