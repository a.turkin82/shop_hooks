import axios from "../../axios";
import * as types from '../actionTypes';

const loginRequest = () => ({ type: types.LOGIN_REQUEST });
const loginSuccess = (payload, token) => ({ type: types.LOGIN_SUCCESS, payload, token });
const loginFailure = error => ({ type: types.LOGIN_FAILURE, error });

export const login = (data, onSuccess, onFailure) => {
  return dispatch => {
    dispatch(loginRequest());
    return axios.post('/users/auth', data).then(
      response => {
        dispatch(loginSuccess(response.data.user, response.data.token));
        if (onSuccess) onSuccess();
      },
      error => {
        dispatch(loginFailure(error));
        if (onFailure) onFailure();
      }
    )
  }
}

export const loginAsGuest = (onSuccess, onFailure) => {
  return dispatch => {
    dispatch(loginRequest());
    return axios.get('/users/auth-as-guest').then(
      response => {
        dispatch(loginSuccess(response.data.user, response.data.token));
        if (onSuccess) onSuccess();
      },
      error => {
        dispatch(loginFailure(error));
        if (onFailure) onFailure();
      }
    )
  }
}

const logoutRequest = () => ({ type: types.LOGOUT_REQUEST });
const logoutSuccess = () => ({ type: types.LOGOUT_SUCCESS });
const logoutFailure = error => ({ type: types.LOGOUT_FAILURE, error });

export const logout = (onSuccess, onFailure) => {
  return dispatch => {
    dispatch(logoutRequest());
    return axios.get('/users/logout').then(
      response => {
        dispatch(logoutSuccess());
        if (onSuccess) onSuccess();
      },
      error => {
        dispatch(logoutFailure(error));
        if (onFailure) onFailure();
      }
    )
  }
}

const authRequest = () => ({ type: types.AUTH_REQUEST });
const authSuccess = payload => ({ type: types.AUTH_SUCCESS, payload });
const authFailure = error => ({ type: types.AUTH_FAILURE, error });

export const auth = (onSuccess, onFailure) => {
  return dispatch => {
    dispatch(authRequest());
    return axios.get('/users/auth').then(
      response => {
        dispatch(authSuccess(response.data));
        if (onSuccess) onSuccess();
      },
      error => {
        dispatch(authFailure(error));
        if (onFailure) onFailure();
      }
    )
  }
}
