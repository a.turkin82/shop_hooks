import axios from "../../axios";
import * as types from '../actionTypes';

const fetchCategoriesListRequest = () => ({ type: types.FETCH_CATEGORIES_LIST_REQUEST });
const fetchCategoriesListSuccess = payload => ({ type: types.FETCH_CATEGORIES_LIST_SUCCESS, payload });
const fetchCategoriesListFailure = error => ({ type: types.FETCH_CATEGORIES_LIST_FAILURE, error });

export const fetchCategoriesList = () => {
  return dispatch => {
    dispatch(fetchCategoriesListRequest());
    return axios.get('/categories').then(
      response => {
        dispatch(fetchCategoriesListSuccess(response.data));
      },
      error => {
        dispatch(fetchCategoriesListFailure(error))
      }
    )
  }
}

const createCategoryRequest = () => ({ type: types.CREATE_CATEGORY_REQUEST });
const createCategorySuccess = () => ({ type: types.CREATE_CATEGORY_SUCCESS });
const createCategoryFailure = error => ({ type: types.CREATE_CATEGORY_FAILURE, error });

export const createCategory = (data, onSuccess) => {
  return dispatch => {
    dispatch(createCategoryRequest());
    return axios.post('/categories', data).then(
      response => {
        dispatch(createCategorySuccess());
        onSuccess();
      },
      error => {
        dispatch(createCategoryFailure(error));
      }
    )
  }
}

const updateCategoryRequest = () => ({ type: types.UPDATE_CATEGORY_REQUEST });
const updateCategorySuccess = () => ({ type: types.UPDATE_CATEGORY_SUCCESS });
const updateCategoryFailure = error => ({ type: types.UPDATE_CATEGORY_FAILURE, error });

export const updateCategory = (id, data, onSuccess) => {
  return dispatch => {
    dispatch(updateCategoryRequest());
    return axios.put(`/categories/${id}`, data).then(
      response => {
        dispatch(updateCategorySuccess());
        onSuccess();
      },
      error => {
        dispatch(updateCategoryFailure(error))
      }
    )
  }
}

const deleteCategoryRequest = () => ({ type: types.DELETE_CATEGORY_REQUEST });
const deleteCategorySuccess = () => ({ type: types.DELETE_CATEGORY_SUCCESS });
const deleteCategoryFailure = error => ({ type: types.DELETE_CATEGORY_FAILURE, error });

export const deleteCategory = (id, onSuccess) => {
  return dispatch => {
    dispatch(deleteCategoryRequest());
    return axios.delete(`/categories/${id}`).then(
      response => {
        dispatch(deleteCategorySuccess());
        onSuccess();
      },
      error => {
        dispatch(deleteCategoryFailure(error))
      }
    )
  }
}
