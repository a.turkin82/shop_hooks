import axios from "../../axios";
import * as types from '../actionTypes';

const fetchColorsListRequest = () => ({ type: types.FETCH_COLORS_LIST_REQUEST });
const fetchColorsListSuccess = payload => ({ type: types.FETCH_COLORS_LIST_SUCCESS, payload });
const fetchColorsListFailure = error => ({ type: types.FETCH_COLORS_LIST_FAILURE, error });

export const fetchColorsList = () => {
  return dispatch => {
    dispatch(fetchColorsListRequest());
    return axios.get('/settings/colors').then(
      response => {
        dispatch(fetchColorsListSuccess(response.data));
      },
      error => {
        dispatch(fetchColorsListFailure(error))
      }
    )
  }
}

const createColorRequest = () => ({ type: types.CREATE_COLOR_REQUEST });
const createColorSuccess = () => ({ type: types.CREATE_COLOR_SUCCESS });
const createColorFailure = error => ({ type: types.CREATE_COLOR_FAILURE, error });

export const createColor = (data, onSuccess) => {
  return dispatch => {
    dispatch(createColorRequest());
    return axios.post('/settings/colors', data).then(
      response => {
        dispatch(createColorSuccess());
        onSuccess();
      },
      error => {
        dispatch(createColorFailure(error));
      }
    )
  }
}

const updateColorRequest = () => ({ type: types.UPDATE_COLOR_REQUEST });
const updateColorSuccess = () => ({ type: types.UPDATE_COLOR_SUCCESS });
const updateColorFailure = error => ({ type: types.UPDATE_COLOR_FAILURE, error });

export const updateColor = (id, data, onSuccess) => {
  return dispatch => {
    dispatch(updateColorRequest());
    return axios.put(`/settings/colors/${id}`, data).then(
      response => {
        dispatch(updateColorSuccess());
        onSuccess();
      },
      error => {
        dispatch(updateColorFailure(error))
      }
    )
  }
}

const deleteColorRequest = () => ({ type: types.DELETE_COLOR_REQUEST });
const deleteColorSuccess = () => ({ type: types.DELETE_COLOR_SUCCESS });
const deleteColorFailure = error => ({ type: types.DELETE_COLOR_FAILURE, error });

export const deleteColor = (id, onSuccess) => {
  return dispatch => {
    dispatch(deleteColorRequest());
    return axios.delete(`/settings/colors/${id}`).then(
      response => {
        dispatch(deleteColorSuccess());
        onSuccess();
      },
      error => {
        dispatch(deleteColorFailure(error))
      }
    )
  }
}
