import {localStorageKey} from "../config";
import store from "./configureStore";

export const saveState = state => {
  try {
    const serializedState = JSON.stringify({token: state.token});
    localStorage.setItem(localStorageKey, serializedState);
  } catch (e) {
    console.log('Could not save state into Local Storage');
  }
};

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem(localStorageKey);
    if (serializedState === null) {
      return undefined;
    }
    return {auth: {...store.getState().auth, ...JSON.parse(serializedState)}};
  } catch (e) {
    return undefined;
  }
};
