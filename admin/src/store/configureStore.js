import {applyMiddleware, combineReducers, compose, legacy_createStore} from "redux";
import thunkMiddleware from 'redux-thunk';

import {loadState, saveState} from "./localStorage";

import authReducer from "./reducers/auth";
import categoriesReducer from "./reducers/categories";
import colorsReducer from "./reducers/colors";
import productsReducer from "./reducers/products";

const rootReducer = combineReducers({
  auth: authReducer,
  categories: categoriesReducer,
  colors: colorsReducer,
  products: productsReducer
});

const enhancers = compose(applyMiddleware(thunkMiddleware));

const persistedState = loadState();

const store = legacy_createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
  saveState(store.getState().auth);
});

export default store;
