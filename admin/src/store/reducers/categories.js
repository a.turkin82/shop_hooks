import * as types from "../actionTypes";

const initialState = {
  categoriesList: [],
  loadingList: false,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case types.FETCH_CATEGORIES_LIST_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.FETCH_CATEGORIES_LIST_SUCCESS:
      return { ...state, categoriesList: action.payload, loadingList: false };
    case types.FETCH_CATEGORIES_LIST_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.CREATE_CATEGORY_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.CREATE_CATEGORY_SUCCESS:
      return { ...state, loadingList: false };
    case types.CREATE_CATEGORY_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.UPDATE_CATEGORY_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.UPDATE_CATEGORY_SUCCESS:
      return { ...state, loadingList: false };
    case types.UPDATE_CATEGORY_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.DELETE_CATEGORY_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.DELETE_CATEGORY_SUCCESS:
      return { ...state, loadingList: false };
    case types.DELETE_CATEGORY_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    default:
      return state;
  }
}

export default reducer;
