import * as types from "../actionTypes";

const initialState = {
  account: {},
  token: null,
  loading: false,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case types.LOGIN_REQUEST:
      return { ...state, error: null, loading: true };
    case types.LOGIN_SUCCESS:
      return { ...state, account: action.payload, token: action.token, loading: false };
    case types.LOGIN_FAILURE:
      return { ...state, error: action.error, loading: false };

    case types.LOGOUT_REQUEST:
      return { ...state, error: null, loading: true };
    case types.LOGOUT_SUCCESS:
      return { ...state, account: {}, token: null, loading: false };
    case types.LOGOUT_FAILURE:
      return { ...state, error: action.error, loading: false };

    case types.AUTH_REQUEST:
      return { ...state, error: null, loading: true };
    case types.AUTH_SUCCESS:
      return { ...state, account: action.payload, loading: false };
    case types.AUTH_FAILURE:
      return { ...state, error: action.error, loading: false };

    default:
      return state;
  }
}

export default reducer;
