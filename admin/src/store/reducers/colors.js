import * as types from "../actionTypes";

const initialState = {
  colorsList: [],
  loadingList: false,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case types.FETCH_COLORS_LIST_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.FETCH_COLORS_LIST_SUCCESS:
      return { ...state, colorsList: action.payload, loadingList: false };
    case types.FETCH_COLORS_LIST_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.CREATE_COLOR_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.CREATE_COLOR_SUCCESS:
      return { ...state, loadingList: false };
    case types.CREATE_COLOR_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.UPDATE_COLOR_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.UPDATE_COLOR_SUCCESS:
      return { ...state, loadingList: false };
    case types.UPDATE_COLOR_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.DELETE_COLOR_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.DELETE_COLOR_SUCCESS:
      return { ...state, loadingList: false };
    case types.DELETE_COLOR_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    default:
      return state;
  }
}

export default reducer;
