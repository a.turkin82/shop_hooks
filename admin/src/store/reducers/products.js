import * as types from "../actionTypes";

const initialState = {
  productsList: [],
  loadingList: false,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case types.FETCH_PRODUCTS_LIST_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.FETCH_PRODUCTS_LIST_SUCCESS:
      return { ...state, productsList: action.payload, loadingList: false };
    case types.FETCH_PRODUCTS_LIST_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.CREATE_PRODUCT_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.CREATE_PRODUCT_SUCCESS:
      return { ...state, loadingList: false };
    case types.CREATE_PRODUCT_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.UPDATE_PRODUCT_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.UPDATE_PRODUCT_SUCCESS:
      return { ...state, loadingList: false };
    case types.UPDATE_PRODUCT_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    case types.DELETE_PRODUCT_REQUEST:
      return { ...state, error: null, loadingList: true };
    case types.DELETE_PRODUCT_SUCCESS:
      return { ...state, loadingList: false };
    case types.DELETE_PRODUCT_FAILURE:
      return { ...state, error: action.error, loadingList: false };

    default:
      return state;
  }
}

export default reducer;
