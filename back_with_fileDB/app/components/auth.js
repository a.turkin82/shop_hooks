const express = require('express');
const uuid = require("uuid");

const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res) => {
    const user = db.getUsers().find(el => el.accessToken === req.headers['x-access-token']);
    if (user) {
      const result = {...user};
      delete result.password;
      res.send(result);
    }
    else res.status(403).send({message: 'Not authorized'});
  });

  router.post('/', (req, res) => {
    const user = db.getUsers().find(el => (el.email === req.body.email && el.password === req.body.password));
    if (user) {
      user.accessToken = uuid.v4();
      db.authentication(user).then(() => {
        const result = {...user};
        delete result.password;
        res.send(result);
      }).catch(error => {
        res.status(error.code).send(error.message);
      });
    }
    else res.status(404).send({message: 'User not found'});
  });

  return router;
}

module.exports = createRouter;
