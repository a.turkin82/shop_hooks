const express = require('express');
const uuid = require('uuid');
const config = require('../../config');

const upload = require('../middlewares/upload');
const stringToBoolean = require("../misc/stringToBoolean");

const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res) => {
    res.send(db.getProducts());
  });

  router.get('/:id', (req, res) => {
    const data = db.getProductsById(req.params.id);
    if (!data) {
      return res.status(404).send({message: 'Not found'});
    }
    res.send(data);
  });

  router.post('/', upload.single('image'), (req, res) => {
    const postData = {
      ...req.body,
      delivery: stringToBoolean(req.body.delivery),
      id: uuid.v4()
    };

    if (req.file) {
      postData.image = config.uploadUrl + req.file.filename;
    }

    db.saveProducts(postData).then(() => {
      res.send({message: 'Writing db successfully!!!'});
    }).catch(error => {
      res.status(error.code).send(error.message);
    });
  });

  router.put('/:id', (req, res) => {
    db.updateProducts(req.params.id, req.body).then(() => {
      res.send({message: 'Writing db successfully!!!'});
    }).catch(error => {
      res.status(error.code).send(error.message);
    });
  });

  router.delete('/:id', (req, res) => {
    db.deleteProducts(req.params.id).then(() => {
      res.send({message: 'Successfully deleted!!!'});
    }).catch(error => {
      res.status(error.code).send(error.message);
    });
  });

  return router;
}

module.exports = createRouter;
