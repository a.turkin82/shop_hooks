const express = require('express');
const uuid = require("uuid");

const router = express.Router();

const createRouter = (db) => {
  router.get('/colors', (req, res) => {
    res.send([
      { id: '1', name: 'Красный' },
      { id: '2', name: 'Синий' },
      { id: '3', name: 'Зеленый' },
      { id: '4', name: 'Черный' },
      { id: '5', name: 'Белый' }
    ]);
  });

  return router;
}

module.exports = createRouter;
