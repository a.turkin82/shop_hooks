const express = require('express');
const uuid = require("uuid");

const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res) => {
    res.send(db.getCategories());
  });

  router.get('/:id', (req, res) => {
    const data = db.getCategoriesById(req.params.id);
    if (!data) {
      return res.status(404).send({message: 'Not found'});
    }
    res.send(data);
  });

  router.post('/', (req, res) => {
    console.log(req.body);
    const postData = {
      ...req.body,
      id: uuid.v4()
    };

    db.saveCategories(postData).then(() => {
      res.send({message: 'Writing db successfully!!!'});
    }).catch(error => {
      res.status(error.code).send(error.message);
    });
  });

  router.put('/:id', (req, res) => {
    db.updateCategories(req.params.id, req.body).then(() => {
      res.send({message: 'Writing db successfully!!!'});
    }).catch(error => {
      res.status(error.code).send(error.message);
    });
  });

  router.delete('/:id', (req, res) => {
    db.deleteCategories(req.params.id).then(() => {
      res.send({message: 'Successfully deleted!!!'});
    }).catch(error => {
      res.status(error.code).send(error.message);
    });
  });

  return router;
}

module.exports = createRouter;
