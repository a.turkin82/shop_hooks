function stringToBoolean (value) {
  const s = String(value);
  return (s === 'true' || s === '1');
  // return !(s === 'false' || s === 'undefined' || s === 'null' || s === 'NaN' || s === '0' || s === '');
}

module.exports = stringToBoolean;
