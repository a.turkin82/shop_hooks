const express = require('express');
const cors = require('cors');

const products = require('./app/components/products');
const categories = require('./app/components/categories');
const settings = require('./app/components/settings');
const auth = require('./app/components/auth');

const fileDb = require('./fileDb');
const app = express();

const port = 8000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

fileDb.init().then(() => {
  app.use('/products', products(fileDb));
  app.use('/categories', categories(fileDb));
  app.use('/settings', settings(fileDb));
  app.use('/auth', auth(fileDb));

  app.listen(port, () => {
    console.log('Server started on ' + port + ' port');
  });
});
