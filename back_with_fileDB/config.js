const path = require('path');
const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadUrl: '/uploads/',
  uploadPath: path.join(rootPath, '/public/uploads')
};
