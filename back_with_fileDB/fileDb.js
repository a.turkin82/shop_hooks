const fs = require('fs');

let products = [];
let categories = [];
let users = [];

function writeFile(fileName) {
  let file;
  if (fileName === 'products') file = products;
  if (fileName === 'categories') file = categories;
  if (fileName === 'users') file = users;
  return new Promise((resolve, reject) => {
    fs.writeFile(`./db/${fileName}.json`, JSON.stringify(file), (err) => {
      if (err) reject({code: 500, message: 'Internal server error'});
      resolve();
    });
  })
}

module.exports = {
  init: () => {
    const initProducts = new Promise((resolve, reject) => {
      fs.readFile('./db/products.json', (err, buffer) => {
        if (err) reject();
        products = JSON.parse(buffer);
        resolve();
      });
    });
    const initCategories = new Promise((resolve, reject) => {
      fs.readFile('./db/categories.json', (err, buffer) => {
        if (err) reject();
        categories = JSON.parse(buffer);
        resolve();
      });
    });
    const initUsers = new Promise((resolve, reject) => {
      fs.readFile('./db/users.json', (err, buffer) => {
        if (err) reject();
        users = JSON.parse(buffer);
        resolve();
      });
    });
    return Promise.all([initProducts, initCategories, initUsers]);
  },
  getProducts: () => products,
  getCategories: () => categories,
  getProductsById: (id) => products.find(el => el.id.toString() === id.toString()),
  getCategoriesById: (id) => categories.find(el => el.id.toString() === id.toString()),
  saveProducts: (payload) => {
    return new Promise((resolve, reject) => {
      if (payload.id && products.some(el => el.id.toString() === payload.id.toString())) {
        reject({code: 409, message: 'This entry already exist'});
      } else {
        products.push(payload);
        writeFile('products').then(() => resolve()).catch(err => reject(err));
      }
    });
  },
  saveCategories: (payload) => {
    return new Promise((resolve, reject) => {
      if (payload.id && categories.some(el => el.id.toString() === payload.id.toString())) {
        reject({code: 409, message: 'This entry already exist'});
      } else {
        categories.push(payload);
        writeFile('categories').then(() => resolve()).catch(err => reject(err));
      }
    });
  },
  updateProducts: (id, payload) => {
    return new Promise((resolve, reject) => {
      const index = products.findIndex(el => el.id.toString() === id.toString());
      if (index < 0) reject({code: 404, message: 'Entry not found'});
      const newData = {...payload};
      delete newData.id;
      products[index] = Object.assign(products[index], newData);
      writeFile('products').then(() => resolve()).catch(err => reject(err));
    });
  },
  updateCategories: (id, payload) => {
    return new Promise((resolve, reject) => {
      const index = categories.findIndex(el => el.id.toString() === id.toString());
      if (index < 0) reject({code: 404, message: 'Entry not found'});
      const newData = {...payload};
      delete newData.id;
      categories[index] = Object.assign(categories[index], newData);
      writeFile('categories').then(() => resolve()).catch(err => reject(err));
    });
  },
  deleteProducts: (id) => {
    return new Promise((resolve, reject) => {
      const index = products.findIndex(el => el.id.toString() === id.toString());
      if (index < 0) reject({code: 404, message: 'Entry not found'});
      console.log(products);
      products.splice(index, 1);
      console.log(products);
      writeFile('products').then(() => resolve()).catch(err => reject(err));
    });
  },
  deleteCategories: (id) => {
    return new Promise((resolve, reject) => {
      const index = categories.findIndex(el => el.id.toString() === id.toString());
      if (index < 0) reject({code: 404, message: 'Entry not found'});
      categories.splice(index, 1);
      writeFile('categories').then(() => resolve()).catch(err => reject(err));
    });
  },
  authentication: (user) => {
    return new Promise((resolve, reject) => {
      const index = users.findIndex(el => el.id.toString() === user.id.toString());
      if (index < 0) reject({code: 404, message: 'Entry not found'});
      users[index] = user;
      writeFile('users').then(() => resolve()).catch(err => reject(err));
    });
  },
  getUsers: () => users
}
